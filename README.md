# gokuai

A Node.JS convenience wrapper around the gokuai API. Simplifies OAuth handshake and removes HTTP ceremony.

## Installation

I always recommend you bundle your dependencies with your application. To do
this, create a `package.json` file in the root of your project with the minimum
information...

    {
      "name": "yourapplication",
      "version": "0.1.0",
      "dependencies": {
        "gokuai": "0.5.7"
      }
    }

Then run the following command using npm...

    npm install

OR, if you just want to start playing with the library run...

    npm install gokuai

## API Overview

`gokuai` methods (where gokuai is set from requiring the gokuai library)...

    app                 <-- creates application object

`app` methods (where app is created from the above `app` call)...

    requesttoken        <-- creates request token for getting request token and authorization url
    getuserinfo         <-- creates access token for creating a client object
    login               <-- this is a wrapper of 'requesttoken' and 'getuserinfo' and will refresh the token by itself
    command             <-- all the command that can operate


`command` methods (where client is created from the above `client` call)...

    mkdir               <-- make directory
    mv                  <-- move file or directory
    cp                  <-- copy file or directory
    rm                  <-- remove file or directory
    rename              <-- rename a filename or a dir
    put                 <-- upload file
    get                 <-- download file
    metadata            <-- get file or directory information
    search              <-- search directory
    thumbnails          <-- get thumbnail of file
    readdir             <-- reads directory

## How to Use

Creating a functional `gokuai` client is a four step process.

1. create an `app` using 'username' and 'password' registered in gokuai and application credentials provided by gokuai.
2. obtain request token to use for get user infomation.
3. get user infomation by call getuserinfo.
4. create a 'command' object

### Step 1

    var gokuai  = require("gokuai")
    var client = new gokuai.app({username:username,password:password,app_key:app_key,app_secret:app_secret,machine:'webdav'});

### Step 2

Authorization is a three step process.

a) Get a request token...

    app.requesttoken(function(err){
      console.log(err)
    })

b) get user infomation with the request token...

    app.getuserinfo(function(err){
      console.log(err)
    })

or you can use app.login(function(err){
       console.log(err)
    }) instead a) and b)

### Step 3

    var command = app.command();

Now we have a client command object that gives us access to all the api functionality.

## Client Methods

### mkdir(path, callback)

Creates directory at specified location.

    command.mkdir("foo", function(err){
      console.log(err)
    })

### mv(from\_path, to\_path, callback)

Moves file or directory to a new location.

    command.mv("foo", "bar", function(err){
      console.log(err)
    })

### cp(from\_path, to\_path, callback)

Copies a file or directory to a new location.

    command.cp("bar", "baz", function(status, reply){
      console.log(reply)
    })

### rm(path, [options,] callback)

Removes a file or directory.

    command.rm("README.txt", function(err){
      console.log(err)
    })

### put(path, filename, callback)

Creates or modifies a file with given filename. `filename` must exist.

    command.put("foo/hello.txt", "tmp.txt", function(err){
      console.log(err)
    })

### createReadStream(path,callback)

Pulls down file (available as a buffer) with its metadata.

    command.createReadStream("foo/hello.txt", function(err, stream){
      stream.on('data',function(data){

      })

      stream.on('end',function(){

      })
    })

### metadata(path, callback)
    command.metadata("Getting_Started.pdf", options, function(status, metadata){
      console.log(metadata)
    })

output of `metadata` returns...

    {
        hash: 文件路径hash
    　　dir: 是否文件夹
    　　fullpath: 文件路径
    　　filename: 文件名称
    　　filehash: 文件hash
    　　filesize: 文件大小
    　　version: 版本号
    　　cmd: 操作状态
    　　last_member_id: 最后操作人ID
    　　last_member_name:最后操作人
    　　last_member_machine:最后操作机器
    　　nosync: 是否设置为不同步
    　　lock: 是否锁定
    　　vcount: 版本数
    　　share: 是否分享
    　　publish: 是否发布
    　　discuss: 备注数
    　　uri: 文件下载地址
    　　uri_local_server:文件下载地址(多地址)
    　　dateline: 最后操作时间
    　　auth:权限(-1预览, 0只读, 1读写, 2共同拥有者, 3拥有者),
    　　accept:当前项是否是接收的
    　　share_counts:共多少个共享参与人和群组(int)
    　　enable_cancel_share:是否有权限取消共享
    　　enable_quit_share:是否有权限退出共享
    　　org_share:是不为团队共享 1为是 0为否
    }

### search(path, query, [options,] callback)

### shares(path, [options,] callback)

### thumbnails(path, [options,] callback)


### readdir(path, callback)

Get an array of paths for all files and directories found in the given path. The method calls recursively to dropbox so it can take a long time to evaluate.

    client.readdir('/', function(status, reply){
        console.log(reply)
    })

Output of `readdir` returns...

    {
        'foo':
        {
           count: 总数
           list:
           　　[
           　　	hash: 文件路径hash
           　　	fullpath: 文件路径
           　　  filename: 文件名称
           　　	filehash: 文件hash
           　　	filesize: 文件大小
           　　	version: 版本号
           　　	cmd: 操作状态
           　　	share: 是否共享
           　　	dir: 是否文件夹
           　　	last_member_id: 最后操作人ID
           　　	last_member_name:最后操作人
           　　	dateline: 最后操作时间
           　　  org_share: 是否为团队共享的 1为是 0为否
           　　]
        } ,

    }

## License

Copyright 2013 gokuai network technology Co.Ltd.
All rights reserved.

